FROM python:3.7.5-slim
RUN python -m pip install DateTime
COPY ./test/helloworld.py /home
CMD ["python", "test/main.py"]